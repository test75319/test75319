<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Datavisualisation </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

    <header>
        <?php include("header.php"); ?>
    </header>

    <main>
        <div class="retour">
            <a href="creations.php">retour</a>
        </div>

        <div class="contenu">
            <h1>Dataviz</h1>

            <p> Réaliser lors de ma formation, le but de ce travail était de nous faire travailler sur la data visualisation. Ici nous devions trouver les informations importantes en rapport avec le sujet. Ensuite il fallait les retranscrire en image afin de faciliter leur compréhension. </p>
            <div class="img_portrait">
                <div><img src="img/dataviz1.png" alt="Dataviz1" /></div>
                <div><img src="img/dataviz2.png" alt="Dataviz2" /></div>
            </div>


            <a class="fin" href="articltiny_house.php">< Projet préccédent</a>
            <a class="fin" href="charte_graphique.php">Projet suivant ></a>

        </div>
    </main>

    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>