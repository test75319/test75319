<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Memphis restaurant </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

    <header>
        <?php include("header.php"); ?>
    </header>

    <main>
        <div class="retour">
            <a href="creations.php">retour</a>
        </div>

        <div class="contenu">
            <h1>Memphis restaurant</h1>

            <p> Maquette réalisée lors de ma 2ème année en DUT MMI. La consigne était de faire la refonte d'un site internet existant afin de le rendre plus ergonomique. Nous devions réalisé la maquette web et mobile de la page d'accueil. </p>
            <div class="img_paysage">
                <div><img src="img/memphis/accueil.png" alt="Memphis maquette web" /></div>
            </div>
            <div class="img_portrait">
                <div><img src="img/memphis/accueil-mobile.png" alt="Memphis maquette mobile" /></div>
                <div><img src="img/memphis/accueil_mobile_bis.png" alt="Memphis maquette mobile"/></div>
            </div>


            <a class="fin" href="weeknd.php">< Projet préccédent</a>
            <a class="fin" href="cap_grand_air.php">Projet suivant ></a>

        </div>
    </main>
    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>