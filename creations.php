<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Créations </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/creation.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	

</head>
<body>

<header>
	<?php include("header.php"); ?>
</header>

<main>
  <div class="contenu">
      <div class="bouton">
        <button onclick="afficher('graphisme')">Graphisme</button>
        <button onclick="afficher('webdesign')">Webdesign</button>
        <button onclick="afficher('tout')">Tout</button>
      </div>
      <div id="wrapper">

        <div id="creations">
            <div class="graphisme">
              <a href="marys.php"><img src="img/marys/logo2.jpg" alt="Projet: Rebranding fictif de Mary's coffee" title="Mary's coffee" />
              <div class="legende">Mary's coffee shop</div></a>
            </div>

            <div class="graphisme">
              <a href="aude.php"><img src="img/aude/miniature1.jpg" alt="Projet :Série d'illustrations" title="Série d'illustrations" />
              <div class="legende">Série d'illustration</div></a>
            </div>

            <div class="graphisme">
              <a href="skater_girl.php"><img src="img/skater_girl.png" alt="Projet :illustration femme avec  des rollers" title="illustration femme avec  des rollers" />
              <div class="legende">Fille sur des rollers</div></a>
            </div>

            
            <div class="graphisme">
              <a href="nuit_saint_jacques.php"><img src="img/nuits_saint_jacques.png" alt="Projet :Affiche fictive Nuits de Saint Jacques 2021" title="Affiche fictive Nuits de Saint Jacques 2021" />
              <div class="legende">Festival Les Nuits de Saint Jacques</div></a>
            </div>

            
            <div class="graphisme">
              <a href="fc.php"><img src="img/affiche_fg.png" alt="Projet : Affiche fictive Fight Club" title="Affiche fictive Fight Club" />
              <div class="legende">Film Fight Club</div></a>
            </div>

            <div class="graphisme">
              <a href="weeknd.php"><img src="img/weeknd.png" alt="Projet :Affiche fictive concert the Weeknd" title="Affiche fictive concert the Weeknd" />
              <div class="legende">Concert the Weeknd</div></a>
            </div>



            <div class="webdesign">
                <a href="memphis.php"><img src="img/memphis/miniature.png" alt="Projet : Refonte fictive site internet Memphis" title="Refonte fictive site internet Memphis" />
                <div class="legende">Maquette site Memphis Restaurant</div></a>
            </div>

            <div class="webdesign">
                <a href="cap_grand_air.php"><img src="img/cap_grand_air/capture.png" alt="Projet :Refonte site internet Cap Grand Air" title="Refonte site internet Cap Grand Air" />
                <div class="legende">Site internet Cap Grand Air</div></a>
            </div>



            <div class="graphisme">
                <a href="theatre.php"><img src="img/affiche_theatre.png" alt="Projet : Affiche de theatre" title="Affiche de theatre" />
                <div class="legende">Affiche de theatre</div></a>
            </div>

            <div class="graphisme">
                <a href="tiny_house.php"><img src="img/tiny_house.png" alt="Projet :Article Tiny house" title="Article Tiny House" />
                <div class="legende">Article Tiny House</div></a>
            </div>

            <div class="graphisme">
                <a href="dataviz.php"><img src="img/dataviz2.png" alt="Projet :Datavisualisation" title="Datavisualisation" />
                <div class="legende">Datavisualisation</div></a>
            </div>



            <div class="graphisme">
                <a href="charte_graphique.php"><img src="img/charte_graphique/charte_graphique.png" alt="Projet : Aim Design charte graphique" title="Aim Design charte graphique" />
                <div class="legende">Aim Design charte graphique</div></a>
            </div>

            <div class="graphisme">
                <a href="poivrier.php"><img src="img/ptut/bon_cadeau.png" alt="Projet : Restaurant le Poivrier" title="Restaurant le Poivrier" />
                <div class="legende">Restaurant le Poivrier</div></a>
            </div>

            <div class="graphisme">
                <a href="zine.php"><img src="img/zine/Manon_BERAUD.png" alt="Projet : 'Cause we're in too deep - Zine" title="'Cause we're in too deep - Zine" />
                <div class="legende">'Cause we're in too deep - Zine</div></a>
            </div>


            <div class="webdesign">
                <a href="site_solenne.php"><img src="img/miniature_site.jpg" alt="Projet : Site internet eco serviettes" title="Site internet" />
                <div class="legende">Site internet fictif</div></a>
            </div>

        </div>
          


        <!--</div> fin div creation-->

        <script>
        function afficher(nomclasse) {
          if(nomclasse=="tout"){
            var children=document.getElementById("creations").children;
            for (var i = 0; i < children.length; i++) {
              children[i].style.display = "inline-flex";
            }
          }
          else{
            var children=document.getElementById("creations").children;
            for (var i = 0; i < children.length; i++) {
              if (children[i].className==nomclasse) {
                children[i].style.display = "inline-flex";
              } else {
                children[i].style.display = "none";
              }
            }
          }
        }

        </script>
    </div> <!--fin div wrapper-->
  </div>
</main>
<footer>
  <?php include("footer.php"); ?>
</footer>
</body>
</html>

<!--
   <script>
    function afficher(nomclasse) {
      if(nomclasse=="tout"){
        var children=document.getElementById("creations").children;
        for (var i = 0; i < children.length; i++) {
          children[i].style.display = "inline-flex";
        }
      }
      else{
        var children=document.getElementById("creations").children;
        for (var i = 0; i < children.length; i++) {
          if (children[i].className==nomclasse) {
            children[i].style.display = "inline-flex";
          } else {
            children[i].style.display = "none";
          }
        }
      }
    }

    </script>
-->