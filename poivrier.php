<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Restaurant le Poivrier </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

    <header>
        <?php include("header.php"); ?>
    </header>

    <main>
        <div class="retour">
            <a href="creations.php">retour</a>
        </div>

        <div class="contenu">
            <h1>Restaurant le Poivrier</h1>

            <p> Réalisé lors de ma première année en DUT MMI, ce projet groupé avait pour but de nous faire découvrir le monde professionnel. Mes missions étaient de réalisées la charte graphique du Poivrier car celle-ci n’était pas clairement définie. Il n’y avait pas de teinte précise pour les couleurs ni de typographies pour les supports graphiques. J’étais aussi en charge de refaire la carte de visite, l’enseigne extérieure ainsi que le bon cadeau. </p>
            <div class="img_portrait">
                <div><img src="img/ptut/logo_blanc.png" alt="Logo version blanche" /></div>
                <div><img src="img/ptut/logo_orange.png" alt="Logo version orange" /></div>
            </div>

            <div class="img_paysage">
                <div><img src="img/ptut/carte_visite.png" alt="carte de visite" /></div>
            </div>

            <div class="img_portrait">
                <div><img src="img/ptut/bon_cadeau.png" alt="Bon cadeau" /></div>
            </div>

            <div class="img_portrait">
                <div><img src="img/ptut/enseigne_grise.png" alt="Enseigne grise" /></div>
                <div><img src="img/ptut/enseigne_marron.png" alt="Enseigne marron" /></div>
            </div>



            <a class="fin" href="charte_graphique.php">< Projet préccédent</a>
            <a class="fin" href="zine.php">Projet suivant ></a>

        </div>
    </main>
    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>