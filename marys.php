<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Mary's Coffee </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

	<header>
		<?php include("header.php"); ?>
	</header>

	<main>
		<div class="retour">
			<a href="creations.php">retour</a>
		</div>

		<div class="contenu">

			<h1>Mary's Coffee - Rebranding fictif </h1>

			<div class="p">
				<p> Rebranding fictif réalisé pendant le confinement. Le but de ce projet était de m'entraîner à réaliser le branding d'une marque. J'ai choisi Mary's coffee car c'est une chaîne locale (Saint-étienne) mais aussi car je trouvais que leur logo n'était pas adapté.</p>
			</div>

			<div class="img_portrait">
				<div><img class="img_petite" src="img/marys/logo1.jpg" alt="Redesign logo Mary's coffee version 1" /></div>
				<div><img class="img_petite" src="img/marys/logo2.jpg" alt="Redesign logo Mary's coffee version 2" /></div>
			</div>

			<div class="img_paysage">
				<img class="img_moyenne" src="img/marys/carte.png" alt="Redesign de la carte de visite" />
				<img class="img_moyenne" src="img/marys/cup.png" alt="Mockup gobelet" />
				<img class="img_moyenne" src="img/marys/sac.jpg" alt="Mockup sac pour les snacks" />
			</div>

			<a class="fin" href="aude.php">Projet suivant ></a>
		</div>
	</main>
	<footer>
		<?php include("footer.php"); ?>
	</footer>

</body>
</html>