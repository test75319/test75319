<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Accueil </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/index.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

<header>
	<?php include("header.php"); ?>
</header>

<main>
	<div class="contenu">
		<p>Graphiste/Web designer.</p>

		<div class="galerie">
			<div>
			<img src="img/ptut/carte_visite.png" alt="carte de visite restaurant le Poivrier">
			</div>
			<div>
				<img src="img/marys/carte.png" alt="carte de visite Mary's Coffee Shop">
			</div>
			<div>
			<img src="img/miniature_site.jpg" alt="miniature site internet fictif">
			</div>
			<div>
				<img src="img/memphis/miniature.png" alt="miniature site internet Cap Grand Air">
			</div>	
		</div>
		<a href="creations.php"><button>Voir plus</button></a>	

		<p>INSTAGRAM</p>
		<div class="insta">
			<div class="container_insta">
				<a href="https://www.instagram.com/p/CA-OmO7qTKc/" target="_blank"><img src="img/insta/marys1.jpg" alt="Mary's coffee" title="Mary's coffee"/></a>
			</div>
			<div class="container_insta">
				<a href="https://www.instagram.com/p/CA-OYilqHdd/" target="_blank"><img src="img/insta/skater.jpg" alt="Fille en roller" title="fille en roller"/></a>
			</div>
			<div class="container_insta">
				<a href="https://www.instagram.com/p/CA-B5htqgsC/" target="_blank"><img src="img/insta/aimdesign1.jpg" alt="branding personnel" title="branding personnel"/></a>
			</div>
		</div>
	</div>
</main>

<footer>
	<?php include("footer.php") ?>
</footer>

</body>
</html>