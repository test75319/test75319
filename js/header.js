const burger = document.querySelector('.burger');
const menu_mobile = document.querySelector('.menu_mobile');

burger.addEventListener('click', () => {
    burger.classList.toggle('active');
    menu_mobile.classList.toggle('isOpen');
});

//DARKMODE
function myFunction() {
    var element = document.body;
    element.classList.toggle("dark-mode");
 }
  

