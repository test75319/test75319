<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | 'Cause we're in too deep </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

    <header>
        <?php include("header.php"); ?>
    </header>

    <main>
        <div class="retour">
            <a href="creations.php">retour</a>
        </div>

        <div class="contenu">
            <h1>'Cause we're in too deep</h1>

            <p> J'ai réalisé ce zine interactif dans le cadre des mes études en DUT MMI. Mon idée était de créer une playlist que l'on pourrait écouter tout en la regardant. Et ce a travers différentes illustrations mais aussi sur les différentes mises en page.  </p>
            <a href="https://fr.calameo.com/read/006556787118367054d13" target="_blank">Voir en entier</a>
            <div class="img_paysage">
                <div><img src="img/zine/Manon_BERAUD3.png" alt="We are Young" /></div>
                <div><img src="img/zine/Manon_BERAUD11.png" alt="We were the same" /></div>
                <div><img src="img/zine/Manon_BERAUD13.png" alt="Run like the river" /></div>
            </div>


            <a class="fin" href="poivrier.php">< Projet préccédent</a>
            <a class="fin" href="site_solenne.php">Projet suivant ></a>

        </div>
    </main>

    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>