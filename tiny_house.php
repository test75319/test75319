<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Tiny house </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

    <header>
        <?php include("header.php"); ?>
    </header>


    <div class="retour">
        <a href="creations.php">retour</a>
    </div>
    <main>
        <div class="contenu">
            <h1>Tiny house</h1>

            <p> Réaliser lors de la semaine journal. Nous devions rédiger un article sur le thème de notre choix et l’illustrer. J’ai choisis comme sujet les tiny houses. </p>
            <a href="img/tiny_house.png" download="Tiny_house_BERAUD_Manon">Télécharger l'article</a>
            <div class="img_portrait">
                <div><img src="img/tiny_house.png" alt="Article tiny house" /></div>
            </div>


            <a class="fin" href="theatre.php">< Projet préccédent</a>
            <a class="fin" href="dataviz.php">Projet suivant ></a>

        </div>
    </main>
    <footer>
        <?php include("footer.php"); ?>
    </footer> 
</body>
</html>