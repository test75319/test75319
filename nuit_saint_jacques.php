<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Les Nuits de Saint Jacques </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

	<header>
		<?php include("header.php"); ?>
	</header>

	<main>
		<div class="retour">
			<a href="creations.php">retour</a>
		</div>

		<div class="contenu">
			<h1>Les Nuits de Saint Jacques</h1>

			<p> Projet fictif réalisé pendant mon temps libre. Lors de la réalisation de cette affiche j'ai décidé de ne pas reprendre le même style que celle des années préccédentes.
				J'ai fais ce choix car dans mes projets "fictifs" j'aime être libre, ici j'ai opté pour du flat design. </p>
			<div class="img_portrait">
				<div><img src="img/nuits_saint_jacques.png" alt="Les Nuits de Saint Jacques" /></div>
			</div>


			<a class="fin" href="skater_girl.php">< Projet préccédent</a>
			<a class="fin" href="fc.php">Projet suivant ></a>

		</div>
	</main>

	<footer>
		<?php include("footer.php"); ?>
	</footer>
</body>
</html>