<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | The Weeknd </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

	<header>
		<?php include("header.php"); ?>
	</header>

	<main>
		<div class="retour">
			<a href="creations.php">retour</a>
		</div>

		<div class="contenu">
			<h1>Affiche de concert</h1>

			<p>Projet réalisé dans le cadre de ma formation. Le but était de réaliser une affiche en utilisant plusieurs méthodes vues sur photoshop.</p>

			<div class="img_portrait">
				<div>
					<img src="img/weeknd.png" alt="Affiche the weeknd" />
				</div>
			</div>

			<a class="fin" href="fc.php">< Projet préccédent</a>
			<a class="fin" href="memphis.php">Projet suivant ></a>

		</div>
	</main>
	<footer>
		<?php include("footer.php"); ?>
	</footer>

</body>
</html>