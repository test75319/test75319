<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | 70's Souffle de liberté </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

	<header>
		<?php include("header.php"); ?>
	</header>

	<main>
		<div class="retour">
			<a href="creations.php">retour</a>
		</div>

		<div class="contenu">
			<h1>70's souffle de liberté</h1>

			<p> Affiche réalisée pour une compagnie de thêatre afin de promouvoir leur nouveau spectacle "70's souffle de liberté". Dans ce projet nous devions d'abord trier les différentes informations mises à notre disposition afin de ne garder que ce qui était essentiel à la compréhension du sujet de la pièce.</p>

			<div class="img_portrait">
				<div><img class="img_moyenne" src="img/affiche_theatre.png" alt="Affiche 70's souffle de liberté" /></div>
			</div>

			<a class="fin" href="cap_grand_air.php">< Projet préccédent</a>
			<a class="fin" href="tiny_house.php">Projet suivant ></a>

		</div>
	</main>
	<footer>
		<?php include("footer.php"); ?>
	</footer>	
</body>
</html>