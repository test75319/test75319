<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | A propos </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/propos.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

    <header>
        <?php include("header.php"); ?>
    </header>
    <main>
        <div class="contenu">
            <div class="bio">

                <div class="img"><img src="img/portrait.JPG" alt="Portrait" title="Portrait" /></div>
                <div class="texte">
                    <h1>A propos de moi</h1>
                    <p>Actuellement en DUT Métiers du Multimédia et de l’Internet au Puy-en-Velay. <br>
                    je me spécialise dans le graphisme ainsi que dans le web design. </p>
                    <p>Depuis petite je m'interesse à l'univers de l'art que se soit réalisant des projets ou simplement <br>
                    en regardant le travail d'autres artistes. <br>
                    Créative j'aime découvrir de nouvelles méthodes de créations ainsi que de nouveaux supports.<br>
                    C'est pour cela que je me suis lancée dans la graphisme ainsi que dans la création de site web.</p>
                    <a href="doc/CV_BERAUD_Manon.pdf" download="CV_BERAUD_Manon">Télécharger mon cv</a>

                </div>
                
            </div>
            
            <div class="exp">
                <h1>Expériences professionnelles</h1>

                <h3>2020/2021 - Interface, projet tuteuré</h3>
                <p>Lors de ma deuxième année en DUT MMI, j'ai pu cette fois réaliser un projet avec 2 autres camarades. Le but était de produire le journal de l'IUT, nous devions choisir une sélection d'articles ainsi que contacter des sponsors afin de participer au financement de l'impression. Nous devions aussi mettre en place la charte graphique du numéro 21. Enfin, nous étions en charge d'aider les futurs étudiants lors de la réalisation du numéro 22.</p>

                <h3>2019/2020 - Le Poivrier, projet tuteuré</h3>
                <p>Lors de ma première année en DUT MMI nous avons réalisé avec 3 autres étudiants un projet tuteuré avec le restaurant le Poivrier situé au Puy-en-Velay. J’étais en charge des différentes prestations graphiques telles que la carte de visite, l’enseigne extérieure et le bon cadeau. Je me suis aussi occupée de mettre à jour la charte graphique.</p>

                <h3>2015 -  Trenta, stage d’observation</h3>
                <p>Durant ce stage j’ai pu observer les différents postes comme infographiste ou bien photographe. L’une de mes tâches était de faire de la veille de photos pour la foire de Saint-Étienne. </p>
            </div>
        </div>

        <div class="contact">
            <h1>Me contacter</h1>
            
            <form method="post">
                <label for="nom">Nom :</label> <br><br>
                <input type="text" name="nom" placeholder="Nom*" required> </br><br>
                <label for="prenom">Prénom :</label><br><br>
                <input type="text" name="prenom" placeholder="Prénom*" required></br><br>
                <label for="email">Email :</label><br><br>
                <input type="email" name="email" placeholder="Email*" required></br><br>
                <label for="message">Entrez votre message :</label><br><br>
                <textarea name="message" placeholder="Message*" required></textarea> </br><br>
                <input type="submit"></br>
            </form>
            <?php
            if(isset($_POST['message'])){
                $entete  = 'MIME-Version: 1.0' . "\r\n";
                $entete .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $entete .= 'From: ' . $_POST['email'] . "\r\n";

                $message = '<h1>Message envoyé depuis la page Contact de ManonBeraud.fr</h1>
                <p><b>Nom : </b>' . $_POST['nom'] . '<br>
                <b>Prénom : </b>' . $_POST['prenom'] . '<br>
                <b>Email : </b>' . $_POST['email'] . '<br>
                <b>Message : </b>' . $_POST['message'] . '</p>';

                $retour = mail('manon.beraud.f@gmail.com', 'Envoi depuis page Contact', $message, $entete);
                if($retour) {
                    echo '<p>Votre message a bien été envoyé.</p>';
                }
            } 
            ?>   
        </div>
    </main> 

    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>