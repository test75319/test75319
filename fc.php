<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Fight Club </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

<header>
	<?php include("header.php"); ?>
</header>

<main>
    <div class="retour">
        <a href="creations.php">retour</a>
    </div>

    <div class="contenu">
        <h1>Fight Club</h1>

        <p> Projet fictif réalisé pour le concour MMI 2020. </p>
        <div class="img_portrait">
            <div><img src="img/affiche_fg.png" alt="Fight Club" /></div>
        </div>


        <a class="fin" href="nuit_saint_jacques.php">< Projet préccédent</a>
        <a class="fin" href="weeknd.php">Projet suivant ></a>

    </div>
</main>

<footer>
    <?php include("footer.php"); ?>
</footer>
</body>
</html>