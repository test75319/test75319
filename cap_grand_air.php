<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Cap Grand Air </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

    <header>
        <?php include("header.php"); ?>
    </header>

    <main>
        <div class="retour">
            <a href="creations.php">retour</a>
        </div>

        <div class="contenu">
            <h1>Cap Grand Air</h1>

            <p> Site internet réalisé lors de ma 2ème année en DUT MMI. Lors de ce travail en groupe nous avions 1 semaine pour créer un site internet ainsi que des réalisations print l'accompagnant. Je me suis occupé de la majorité du developemment front-end. J'ai aussi participé aux réalisations prints (bannière facebook, affiche trail).</p>
            <a href="http://capgrandair.gwendalbiotteau.fr/" target="_blank">http://capgrandair.gwendalbiotteau.fr/</a>
            <div class="img_paysage">
                <div><img src="img/cap_grand_air/capture.png" alt="Miniature Cap Grand Air" /></div>
                <div><img src="img/cap_grand_air/banniere_FB.png" alt="Bannière Facebook Cap Grand Air" /></div>
            </div>
            <div class="img_portrait">
                <div><img src="img/cap_grand_air/affiche.png" alt="Affiche trail Cap Grand Air" /></div>
            </div>


            <a class="fin" href="memphis.php">< Projet préccédent</a>
            <a class="fin" href="theatre.php">Projet suivant ></a>

        </div>
    </main>
    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>