<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Aim Design charte graphique </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

<header>
	<?php include("header.php"); ?>
</header>

<main>
    <div class="retour">
        <a href="creations.php">retour</a>
    </div>

    <div class="contenu">
        <h1>Aim Design charte graphique</h1>

        <p> Charte graphique réalisée lors de ma première année en DUT MMI. Ici nous devions faire notre personnal branding. </p>
        <a href="https://fr.calameo.com/read/006556787827b2a69fb0a" target="_blank">Voir en entier</a>
        <div class="img_portrait">
            <div><img src="img/charte_graphique/charte_graphique.png" alt="Couverture de la Charte graphique" /></div>
            <div><img src="img/charte_graphique/charte_graphique4.png" alt="logo de la Charte graphique" /></div>
        </div>

        <div class="img_portrait">
            <div><img src="img/charte_graphique/charte_graphique7.png" alt="page typos Charte graphique" /></div>
            <div><img src="img/charte_graphique/charte_graphique8.png" alt="page couleurs Charte graphique" /></div>
        </div>


        <a class="fin" href="dataviz.php">< Projet préccédent</a>
        <a class="fin" href="poivrier.php">Projet suivant ></a>

    </div>
</main>

<footer>
    <?php include("footer.php"); ?>
</footer>
</body>
</html>