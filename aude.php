<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Série d'illustration </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

<header>
	<?php include("header.php"); ?>
</header>

<main>
	<div class="retour">
		<a href="creations.php">retour</a>
	</div>

	<div class="contenu">
		<h1>Série d'illustrations</h1>

		<p> Série d'illustrations print réalisée pour un particulier. </p>
		<div class="img_portrait">
			<div><img src="img/aude/cactus.png" alt="illustration cactus" /></div>
			<div><img src="img/aude/porte.png" alt="illustration porte" /></div>
		</div>
		<div class="img_paysage">
			<div><img src="img/aude/dromadaires.png" alt="illustration dromadaire" /></div>
			<div><img src="img/aude/desert.png" alt="illustration desert" /></div>
		</div>

		<div class="bouton">
		<a class="fin" href="marys.php">< Projet préccédent</a>
		<a class="fin" href="skater_girl.php">Projet suivant ></a>
		</div>

	</div>
</main>

<footer>
	<?php include("footer.php"); ?>
</footer>
</body>
</html>