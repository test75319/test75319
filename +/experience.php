<!DOCTYPE html>
<html>
<head>
	<title>Expérience</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="style.css" />
</head>
<body>
<div class="header">
	<nav class="menu">
		<ul>
			<li><a href="index.php">Accueil</a></li>
			<li><a href="experience.php">Experience</a></li>
			<li><a href="travail.php">Mon travail</a></li>
			<li><a href="contact.php">Contact</a></li>
		</ul>
	</nav>
	<h1>Manon Beraud</h1>
	<div>
		<a href="https://www.instagram.com/mnbrd09/">
		  <img src="img/instagram.png" alt="lien compte instagram" />
		</a>
		<a href="https://www.linkedin.com/in/manon-beraud-73ba291a4/">
		  <img src="img/linkedin.png" alt="lien compte linkedin" />
		</a>
	</div>
</div>
<div class="travail">
	<div class="galerie">
		<a href="marys.php"><img src="img/marys/carte.png" alt="Mary's coffee" title="Mary's coffee" /></a>
		<a href="xavier.php"><img src="img/xavier/carte.png" alt="Xavier Charre coach personnel" title="Xavier Charre - coach personnel" /></a>
		<a href="aude.php"><img src="img/aude/miniature.jpg" alt="Illustration" title="illustration" /></a>
		<img src="img/xavier/carte.png" alt="Xavier Charre coach personnel" title="Xavier Charre - coach personnel" />
		<img src="img/marys/carte.png" alt="Mary's coffee" title="Mary's coffee" />
		<img src="img/xavier/carte.png" alt="Xavier Charre coach personnel" title="Xavier Charre - coach personnel" />
		<img src="img/marys/carte.png" alt="Mary's coffee" title="Mary's coffee" />
		<img src="img/xavier/carte.png" alt="Xavier Charre coach personnel" title="Xavier Charre - coach personnel" />
		<img src="img/marys/carte.png" alt="Mary's coffee" title="Mary's coffee" />
	</div>
</div>

<div class="footer">
	<p>Me contacter ?</p>
	<p>manon.beraud.f@gmail.com</p>
	<p>Manon Beraud - 2020 ©</p>
	
</div>
</body>
</html>