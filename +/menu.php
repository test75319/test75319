<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | header</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/menu.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
	
	
</head>
<body>

    <div class="burger">
		<span></span>
	</div>

	<nav class="menu_mobile">
		<ul>
			<li><a href="index.php">Accueil</a></li>
			<li><a href="propos.php">A propos</a></li>
			<li><a href="creations.php" >Mes créations</a></li>
		</ul>
	</nav>

    <script src="//code.jquery.com/jquery-3.5.1.js"
			  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
			  crossorigin="anonymous"></script>
    <script src="js/header.js"></script>

</body>
</html>
