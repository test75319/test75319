<!DOCTYPE html>
<html>
<head>
	<title>Expérience</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="style.css" />
</head>
<body>

<div class="header">
	<nav class="menu">
        <ul>
            <li><a href="index.php">Accueil</a></li>
            <li><a href="experience.php">A propos</a></li>
            <li><a href="travail.php">Mes créations</a>
                <ul>
                    <li><a href="graphisme.php">Graphisme</a></li>
                    <li><a href="webdesign.php">Web Design</a></li>
                </ul>
            </li>
        </ul>
    </nav>
	<h1>Manon Beraud</h1>
	<div>
		<a href="https://www.instagram.com/mnbrd09/">
		  <img src="img/instagram.png" alt="lien compte instagram" />
		</a>
		<a href="https://www.linkedin.com/in/manon-beraud-73ba291a4/">
		  <img src="img/linkedin.png" alt="lien compte linkedin" />
		</a>
	</div>
</div>


<div class="contact">
	<h1>Me contacter</h1>
	
    <form method="post">
        <label>Nom*</label> </br>
        <input type="text" name="nom" required> </br>
        <label>Prénom*</label> </br>
        <input type="text" name="nom" required></br>
        <label>E-mail*</label> </br>
        <input type="email" name="email" required></br>
        <label>Message*</label></br>
        <textarea name="message" required></textarea> </br>
        <input type="submit"></br>
    </form>
    <?php
    if(isset($_POST['message'])){
        $entete  = 'MIME-Version: 1.0' . "\r\n";
        $entete .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $entete .= 'From: ' . $_POST['email'] . "\r\n";

        $message = '<h1>Message envoyé depuis la page Contact de ManonBeraud.fr</h1>
        <p><b>Nom : </b>' . $_POST['nom'] . '<br>
        <b>Prénom : </b>' . $_POST['prenom'] . '<br>
        <b>Email : </b>' . $_POST['email'] . '<br>
        <b>Message : </b>' . $_POST['message'] . '</p>';

        $retour = mail('manon.beraud.f@gmail.com', 'Envoi depuis page Contact', $message, $entete);
        if($retour) {
            echo '<p>Votre message a bien été envoyé.</p>';
        }
    } 
    ?>   
</div>


<div class="footer">
	<p>Me contacter ?</p>
	<p>manon.beraud.f@gmail.com</p>
	<p>Manon Beraud - 2020 ©</p>	
</div>

</body>
</html>