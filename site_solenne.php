<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Eco serviette </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	

</head>
<body>
	<header>
		<?php include("header.php"); ?>
	</header>

	<main>
		<div class="retour">
			<a href="creations.php">retour</a>
		</div>

		<div class="contenu">
			<h1>Eco serviette</h1>

			<p>Maquette d'un site fictif réalisé dans le cadre de ma formation. Les consignes étaient de réaliser la maquette d'un site proposé par l'un de nos camarades.</p>

			<div class="img_paysage">
				<div><img src="img/site_solenne.jpg" alt="maquette Eco serviette" /></div>
			</div>

			<a class="fin" href="zine.php">< Projet préccédent</a>

		</div>
	</main>

	<footer>
		<?php include("footer.php"); ?>
	</footer>
</body>
</html>