<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MANON BERAUD | Fille sur des rollers </title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/projet.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;800" rel="stylesheet">
	


</head>
<body>

	<header>
		<?php include("header.php"); ?>
	</header>

	<main>
		<div class="retour">
			<a href="creations.php">retour</a>
		</div>

		<div class="contenu">
			<h1>Fille sur des rollers</h1>

			<p> Réalisée pendant mon temps libre. Je souhaitais m’entraîner à l’illustrations. J’ai joué sur les effets de dégradés et avec l’outil plume. </p>
			<div class="img_portrait">
				<div><img src="img/skater_girl.png" alt="Fille sur des rollers" /></div>
			</div>


			<a class="fin" href="aude.php">< Projet préccédent</a>
			<a class="fin" href="nuit_saint_jacques.php">Projet suivant ></a>

		</div>
	</main>

	<footer>
		<?php include("footer.php"); ?>
	</footer>
</body>
</html>